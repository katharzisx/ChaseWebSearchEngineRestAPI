package com.wse.chasewse.rest.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.jboss.logging.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;

import com.wse.chasewse.model.AdvRegisterData;
import com.wse.chasewse.model.repo.AdvertisementRepository;
import com.wse.chasewse.rest.exception.AdvertiserNotFoundException;
import com.wse.chasewse.rest.helper.ProcessQuery;

@RestController
@RequestMapping(value = "/chasewse/ads")

public class AdvertisementController extends BaseController{

	@Autowired
	AdvertisementRepository ad_repository;

	//http://localhost:8080/chase/chasewse/ads/getAllAds
	@RequestMapping(method = RequestMethod.GET, value="/getAllAds")
	@ResponseBody
	Collection<AdvRegisterData> getAllAdvertisements(){
		ArrayList<AdvRegisterData> liAdData = new ArrayList<>();
		for(AdvRegisterData adv_data : ad_repository.findAll()){
			liAdData.add(adv_data);
		}
		return liAdData;
	}

	//http://localhost:8080/chase/chasewse/ads/MCD/getAds
	@RequestMapping(method = RequestMethod.GET, value="/{org}/getAds")
	@ResponseBody
	Collection<AdvRegisterData> getOrganizationAds(@PathVariable String org){
		ArrayList<AdvRegisterData> liAdData = (ArrayList<AdvRegisterData>) ad_repository.findByOrganization(org);
		return liAdData;
	}

	/**
	 * http://localhost:8080/chase/chasewse/ads/register
	* {
		"ad_budget":	,
		"ad_click_rate":	,
		"ad_current_budget":	,
		"ad_display_text": "",
		"ad_id":,
		"ad_img_url":	"",
		"ad_page_url": "",	
		"keywords":"",	
		"organization":""	
		}
	 * @param input
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value="/register")
	public String registerAdvertisement(@RequestBody AdvRegisterData input){
		String strOrganizationName = input.getOrganization();
		String strPageToNavigate= input.getAd_page_url();
		String strDisplayText= input.getAd_display_text();
		String strImageURL= input.getAd_img_url();
		String strKeywords= input.getKeywords();
		String strBudget= String.valueOf(input.getAd_budget());
		String strClickRate = String.valueOf(input.getAd_click_rate());

		HashMap<String, String> mapAD_DATA = new HashMap<>();
		ProcessQuery objProcessQuery = new ProcessQuery();
		objProcessQuery.createDataBaseConnection();

		//ADD TO MAP
		mapAD_DATA.put("ORG", strOrganizationName);
		mapAD_DATA.put("PAGE", strPageToNavigate);
		mapAD_DATA.put("TEXT", strDisplayText);
		mapAD_DATA.put("IMAGE", strImageURL);
		mapAD_DATA.put("KEYWORDS", strKeywords);
		mapAD_DATA.put("BUDGET", strBudget);
		mapAD_DATA.put("RATE", strClickRate);

		//Insert Values To DataBase
		objProcessQuery.insertAdvertisementDataToDB(mapAD_DATA);

		return "Advertisement Registered.";
	}

	/**
	 * url: http://localhost:8080/chase/chasewse/ads/delete/theverge
	 * @param orgname
	 * @return
	 */
	@RequestMapping(value="/delete/{orgname}")
	public String deleteAdvData(@PathVariable String orgname){
		ArrayList<AdvRegisterData> adDataToDelete = (ArrayList<AdvRegisterData>) ad_repository.findByOrganization(orgname);
		if(adDataToDelete!=null && adDataToDelete.size()>0){
			ad_repository.delete(adDataToDelete);
			return "Advertiser/Ads Deleted.";
		}else{
			//return "Advertiser/Ads does not exist.";
			throw new AdvertiserNotFoundException(orgname);
		}	
	}




}
