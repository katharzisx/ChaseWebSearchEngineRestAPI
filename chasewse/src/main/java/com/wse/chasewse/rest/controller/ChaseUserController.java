package com.wse.chasewse.rest.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.wse.chasewse.model.ChaseUser;
import com.wse.chasewse.model.repo.ChaseUserRepository;

@RestController
@RequestMapping(value = "/user")
public class ChaseUserController extends BaseController{
	
	@Autowired
	ChaseUserRepository chaseUserRepository;

	//http://localhost:8080/chase/user/getAll
	@RequestMapping(method = RequestMethod.GET, value="/getAll")
	public Collection<ChaseUser> getAllUserData(){
		ArrayList<ChaseUser> liAllUsers = (ArrayList<ChaseUser>)chaseUserRepository.findAll();
		return liAllUsers;
	}
	
	//http://localhost:8080/chase/user/getUser/test@email.com
	@RequestMapping(method = RequestMethod.GET, value="/getUser/{email}")
	public ChaseUser getAllUserData(@PathVariable String email){
		ChaseUser userData = chaseUserRepository.findByEmail(email);
		return userData;
	}
	
	//http://localhost:8080/chase/user/register
	@RequestMapping(method = RequestMethod.POST, value = "/register")
	public String userRegistration(@RequestBody ChaseUser input){
		if(input!=null && input.getEmail()!=null && input.getUsername()!=null && input.getPassword()!=null){
			ArrayList<ChaseUser> allUsers = (ArrayList<ChaseUser>) chaseUserRepository.findAll();
			Long maxId = 0L;
			if(allUsers!=null && allUsers.size()>0){
				ArrayList<Long> userIDs = new ArrayList<>();
				for (ChaseUser chaseUser : allUsers) {
					userIDs.add(chaseUser.getUser_id());
				}
				maxId = Collections.max(userIDs);
				++maxId;
			}
			input.setUser_id(maxId);
			chaseUserRepository.save(input);
			
		}else{
			return "Enter required values!";
		}
		return "Registration successful!";
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value="/delete/{email}")
	public String deleteUser(@PathVariable String email){
		ChaseUser userData = chaseUserRepository.findByEmail(email);
		chaseUserRepository.delete(userData);
		return "User Deleted!";
	}
	

}
