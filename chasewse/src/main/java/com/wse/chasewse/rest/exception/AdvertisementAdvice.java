package com.wse.chasewse.rest.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.hateoas.VndErrors;
import org.springframework.http.HttpStatus;

@ControllerAdvice
public class AdvertisementAdvice {
	@ResponseBody
	@ExceptionHandler(AdvertiserNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	VndErrors userNotFoundExceptionHandler(AdvertiserNotFoundException ex) {
		return new VndErrors("error", ex.getMessage());
	}
}
