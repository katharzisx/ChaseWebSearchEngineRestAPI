package com.wse.chasewse.rest.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.servlet.RequestDispatcher;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.wse.chasewse.util.GlobalUtilities;
import com.wse.chasewse.model.*;
import com.wse.chasewse.rest.helper.SearchHelper;

import edu.smu.tspell.wordnet.WordNetDatabase;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/chasewse")
public class SearchEngineController extends BaseController{
	

	public SearchEngineController() {
	
	}
	/**
	 * Method to process Text queries
	 * @param searchQuery
	 * @param k : number of documents to be fetched
	 * @param scoringModel:1,2,3,4
	 * @param lang: EN, DE
	 * @param resultType : JSON
	 * @return String
	 */
	//http://localhost:8080/chase/chasewse/query/text/merkel?k=20&model=1&lang=de&resultType=json
	//http://localhost:8080/chase/chasewse/query/text/burger%20playstation?k=20&model=1&lang=de&resultType=json
	@RequestMapping(method = RequestMethod.GET, value="/query/text/{searchQuery}")
	 @ApiResponses(value = { 
	            @ApiResponse(code = 200, message = "Success", response = ResponseResultData.class),
	            @ApiResponse(code = 401, message = "Unauthorized"),
	            @ApiResponse(code = 403, message = "Forbidden"),
	            @ApiResponse(code = 404, message = "Not Found"),
	            @ApiResponse(code = 500, message = "Failure")}) 
	ResponseResultData executeSearch(@PathVariable String searchQuery,
			@RequestParam("k") int k,@RequestParam("model") int scoringModel, 
			@RequestParam("lang") String lang, @RequestParam("resultType") String resultType){
		
		GlobalUtilities.PrintLog("############################# TEXT SEARCH ENGINE START #############################");
				
		QueryData searchRequestObject = new QueryData(searchQuery, k, scoringModel, lang, resultType);
		GlobalUtilities.PrintLog(""+searchRequestObject.toString());
		//Process Query Object
		//Search Helper class
		SearchHelper objSearchHelper = new SearchHelper();
		ResponseResultData jsonResponse = objSearchHelper.processSearchQuery(searchRequestObject);
		return jsonResponse;
	}
	/**
	 * Method to process Image queries
	 * @param searchQuery
	 * @param k
	 * @return Collection
	 */
	//http://localhost:8080/chase/chasewse/query/image/football?k=10
	@RequestMapping(method = RequestMethod.GET, value="/query/image/{searchQuery}")
	Collection<ImageResultData> executeImageSearch(@PathVariable String searchQuery, @RequestParam("k") int k){
		
		GlobalUtilities.PrintLog("############################# IMAGE SEARCH ENGINE START #############################");
		
		SearchHelper objSearchHelper = new SearchHelper();
		//Process Query Object
		ArrayList<ImageResultData> arLiResponse= objSearchHelper.processImageSearch(searchQuery, k);
		return arLiResponse;
	}
}
