package com.wse.chasewse.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class AdvertiserNotFoundException extends RuntimeException {

	public AdvertiserNotFoundException(String advId) {
		super("ADVERTISER Does not exist. ADV_NAME:  '" + advId + "'.");
	}
}