package com.wse.chasewse.rest.helper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.wse.chasewse.util.GlobalUtilities;
import com.wse.chasewse.model.*;


public class ProcessQuery {

	Connection dbConnectionObj = null;
	String HOSTNAME_DBPORT = "127.0.0.1:5433";
	String DATABASE = "DBNAME";
	// DB Credentials
	String USER = "******";
	String PASSWORD = "******";
	// Connection String
	String CONNECTION_URL_STRING = "jdbc:postgresql://" + HOSTNAME_DBPORT + "/" + DATABASE;
	String POSTGRES_JDBC_DRIVER_STRING = "org.postgresql.Driver";

	public ProcessQuery() {
		//
	}

	/**
	 * Method to Establish Connection To ISProject Database
	 */
	public void createDataBaseConnection() {
		try {
			Class.forName(POSTGRES_JDBC_DRIVER_STRING).newInstance();
			// PostGreSql Connection Properties
			Properties connectionProperties = new Properties();
			connectionProperties.setProperty("user", USER);
			connectionProperties.setProperty("password", PASSWORD);
			// DB GetConnection
			// dbConnectionObj
			// =DriverManager.getConnection("jdbc:postgresql://localhost:5432/ISPROJECT","project","armetion");
			dbConnectionObj = DriverManager.getConnection(CONNECTION_URL_STRING, USER, PASSWORD);
			GlobalUtilities.PrintLog("PostGreSQL ISProject DB Connection Successful");
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Method to fetch Query Stats
	 * 
	 * @param searchString
	 * @return
	 */
	public ArrayList<Stat> getStat(ArrayList<String> searchString) {
		ArrayList<Stat> statList = new ArrayList<Stat>();
		for (int i = 0; i < searchString.size(); i++) {
			try {
				PreparedStatement stmt = dbConnectionObj
						.prepareStatement("select distinct term,df from features where term=?");
				stmt.setString(1, searchString.get(i));
				ResultSet rs = stmt.executeQuery();
				while (rs.next()) {
					Stat statobj = new Stat();
					statobj.setTerm(rs.getString(1));
					statobj.setDf(rs.getInt(2));
					statList.add(statobj);
				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println("error in getstat....:" + e.getMessage());
			}

		}
		return statList;

	}

	/**
	 * Method to fetch Total term Count
	 * 
	 * @return
	 */
	public int getCW() {
		int cw = 0;
		try {
			PreparedStatement stmt = dbConnectionObj.prepareStatement("select sum(term_frequency) from features");
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				cw = rs.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("error in getstat....:" + e.getMessage());
		}

		return cw;

	}

	/**
	 * Method for Conjunctive Keyword Term Search
	 * 
	 * @param searchString
	 * @param keywords
	 * @param domain
	 * @param k
	 * @param model
	 * @return
	 */
	public ArrayList<ResultData> keywordSearch(String searchString, String keywords, String domain, int k,
			String queryLang, int model) {
		String domainconcat = "%" + domain + "%";
		ArrayList<ResultData> resultList = new ArrayList<ResultData>();
		try {
			String query = "select * from ( "
					+ "select a.docid as docid, a.score as score, a.rank as rank,a.url as url from ( "
					+ "select * from getTopK(string_to_array(?,' '),?,2,string_to_array(?,' '),?))a " + "inner join "
					+ "(select * from getTopK(string_to_array(?,' '),?,1,string_to_array(?,' '),?))b "
					+ "on a.docid=b.docid) c where docid in ( "
					+ "select doc_id from documents where doc_url like ?) order by rank limit ?";
			// PreparedStatement
			// psmt=databaseConnection1.prepareStatement("select * from
			// getTopK(string_to_array(?,' '),?,1)");
			PreparedStatement psmt = dbConnectionObj.prepareStatement(query);
			psmt.setString(1, searchString);
			psmt.setInt(2, k);
			psmt.setString(3, queryLang);
			psmt.setInt(4, model);
			psmt.setString(5, keywords);
			psmt.setInt(6, k);
			psmt.setString(7, queryLang);
			psmt.setInt(8, model);
			psmt.setString(9, domainconcat);
			psmt.setInt(10, k);

			ResultSet rs = psmt.executeQuery();

			System.out.println("keyword search executed successfully");
			while (rs.next()) {
				ResultData tempData = new ResultData();
				tempData.setScore(rs.getDouble(2));
				tempData.setRank(rs.getInt(3));
				tempData.setUrl(rs.getString(4));

				resultList.add(tempData);
			}
			rs.close();
			// stmt.close();

		} catch (SQLException e) {
			System.out.println("Error in keywordSearch.....:" + e.getMessage());
		}

		return resultList;
	}

	/**
	 * Method for Disjunctive Term Search
	 * 
	 * @param searchString
	 * @param keywords
	 * @param domain
	 * @param k
	 * @param model
	 * @return
	 */
	public ArrayList<ResultData> normalSearch(String searchString, String domain, int k, String queryLang, int model) {
		String domainconcat = "%" + domain + "%";
		ArrayList<ResultData> resultList = new ArrayList<ResultData>();
		try {
			String query = "select * from getTopK(string_to_array(?,' '),?,2,string_to_array(?,' '),?) where docid in ( "
					+ "select doc_id from documents where doc_url like ?) " + "order by rank limit ?";
			// PreparedStatement
			// psmt=databaseConnection1.prepareStatement("select * from
			// getTopK(string_to_array(?,' '),?,1)");
			PreparedStatement psmt = dbConnectionObj.prepareStatement(query);
			psmt.setString(1, searchString);
			psmt.setInt(2, k);
			psmt.setString(3, queryLang);
			psmt.setInt(4, model);
			psmt.setString(5, domainconcat);
			psmt.setInt(6, k);

			ResultSet rs = psmt.executeQuery();

			System.out.println("Normal search executed successfully");
			while (rs.next()) {
				ResultData tempData = new ResultData();
				tempData.setScore(rs.getDouble(2));
				tempData.setRank(rs.getInt(3));
				tempData.setUrl(rs.getString(4));

				resultList.add(tempData);
			}

			rs.close();
			// stmt.close();

		} catch (SQLException e) {
			System.out.println("Error in normalSearch.....:" + e.getMessage());
		}

		return resultList;
	}

	/**
	 * Method for Disjunctive Term Search for Snippets
	 * 
	 * @param searchString
	 * @param keywords
	 * @param domain
	 * @param k
	 * @param model
	 * @return
	 */
	public ArrayList<ResultData> snippetNormalSearch(String searchString, String domain, int k, String queryLang,
			int model) {
		String domainconcat = "%" + domain + "%";
		ArrayList<ResultData> resultList = new ArrayList<ResultData>();
		try {
			String query = "select * from getRankedSnippets(string_to_array(?,' '),?,2,string_to_array(?,' '),?) where doc_id_res in ( "
					+ "select doc_id from documents where doc_url like ?) and score_res>0.0 "
					+ "order by rank_res limit ?";
			// PreparedStatement
			// psmt=databaseConnection1.prepareStatement("select * from
			// getTopK(string_to_array(?,' '),?,1)");
			PreparedStatement psmt = dbConnectionObj.prepareStatement(query);
			psmt.setString(1, searchString);
			psmt.setInt(2, k);
			psmt.setString(3, queryLang);
			psmt.setInt(4, model);
			psmt.setString(5, domainconcat);
			psmt.setInt(6, k);

			ResultSet rs = psmt.executeQuery();

			System.out.println("Snippet Normal search executed successfully");
			while (rs.next()) {
				ResultData tempData = new ResultData();
				tempData.setScore(rs.getDouble(2));
				tempData.setRank(rs.getInt(3));
				tempData.setUrl(rs.getString(4));
				tempData.setDescription(rs.getString(5));
				tempData.getMissingTermsFromResult(searchString, rs.getString(6));

				resultList.add(tempData);
			}
			GlobalUtilities.PrintLog("Snippet REsultlist Size:" + resultList.size());
			rs.close();
			// stmt.close();

		} catch (SQLException e) {
			System.out.println("Error in Snippet normalSearch.....:" + e.getMessage());
		}

		return resultList;
	}

	/**
	 * Method for Conjunctive Keyword Term Search for snippet
	 * 
	 * @param searchString
	 * @param keywords
	 * @param domain
	 * @param k
	 * @param model
	 * @return
	 */
	public ArrayList<ResultData> snippetKeywordSearch(String searchString, String keywords, String domain, int k,
			String queryLang, int model) {
		String domainconcat = "%" + domain + "%";
		ArrayList<ResultData> resultList = new ArrayList<ResultData>();

		try {
			String query = "select * from ( "
					+ "select a.doc_id_res as docid, a.score_res as score, a.rank_res as rank,a.url_res as url,a.snippets_res as snippets,a.occured_term_res as occured_term from ( "
					+ "select * from getRankedSnippets(string_to_array(?,' '),?,2,string_to_array(?,' '),?))a "
					+ "inner join "
					+ "(select * from getRankedSnippets(string_to_array(?,' '),?,1,string_to_array(?,' '),?))b "
					+ "on a.doc_id_res=b.doc_id_res) c where docid in ( "
					+ "select doc_id from documents where doc_url like ?) and score>0.0 order by rank limit ?";
			// PreparedStatement
			// psmt=databaseConnection1.prepareStatement("select * from
			// getTopK(string_to_array(?,' '),?,1)");
			PreparedStatement psmt = dbConnectionObj.prepareStatement(query);
			psmt.setString(1, searchString);
			psmt.setInt(2, k);
			psmt.setString(3, queryLang);
			psmt.setInt(4, model);
			psmt.setString(5, keywords);
			psmt.setInt(6, k);
			psmt.setString(7, queryLang);
			psmt.setInt(8, model);
			psmt.setString(9, domainconcat);
			psmt.setInt(10, k);

			ResultSet rs = psmt.executeQuery();

			System.out.println("Snippet keyword search executed successfully");
			while (rs.next()) {
				ResultData tempData = new ResultData();
				tempData.setScore(rs.getDouble(2));
				tempData.setRank(rs.getInt(3));
				tempData.setUrl(rs.getString(4));
				tempData.setDescription(rs.getString(5));
				tempData.getMissingTermsFromResult(searchString, rs.getString(6));

				resultList.add(tempData);
			}
			rs.close();
			// stmt.close();

		} catch (SQLException e) {
			System.out.println("Error in Snippet keywordSearch.....:" + e.getMessage());
		}

		return resultList;
	}

	/**
	 * DB Method for Image Search
	 * 
	 * @param searchString
	 * @param k
	 */
	public ArrayList<ImageResultData> imageSearchDB(String searchString, int k) {
		ArrayList<ImageResultData> resultList = new ArrayList<ImageResultData>();
		try {
			GlobalUtilities.PrintLog("Image Search Query started " + searchString);
			String imgQuery = "select * from gettopimagek(string_to_array(?,' '),?)";

			PreparedStatement psImgStmnt = dbConnectionObj.prepareStatement(imgQuery);
			psImgStmnt.setString(1, searchString);
			psImgStmnt.setInt(2, k);

			ResultSet rs = psImgStmnt.executeQuery();
			GlobalUtilities.PrintLog("Image Search Query executed successfully");
			while (rs.next()) {
				ImageResultData tempData = new ImageResultData();
				tempData.setDoc_id(rs.getString(1));
				tempData.setImg_id(rs.getString(2));
				tempData.setScore(rs.getDouble(3));
				tempData.setRank(rs.getInt(4));
				tempData.setImg_url(rs.getString(5));
				tempData.setDoc_url(rs.getString(6));
				// TEMP CODE To remove
				// SOME IMAGES like unnecessary icons
				// To remove some images with /// in the URL giving issues
				if (tempData.getImg_url().contains("///")) {
					// Remove the /// occurrence and append it to domain
				} else {
					// Do not add URL with // occurring multiple times
					String sURL = tempData.getImg_url();
					String sURLCheck[] = sURL.split("//");
					if (sURLCheck.length > 2) {
						GlobalUtilities.PrintLog("URL sURLCheck size : " + sURLCheck.length);
						GlobalUtilities.PrintLog("URL CHECK : " + sURL);
					} else {
						resultList.add(tempData);
					}

				}

			}
			// GlobalUtilities.PrintLog("IMAGE result set size :
			// "+resultList.size());
			// CREATE type img_holder as (doc_id text,img_id text, score real,
			// rank int,img_url text, doc_url text);
			rs.close();
		} catch (Exception e) {
			e.getStackTrace();
			GlobalUtilities.PrintLog("Exception in imageSearchDB()" + e.getMessage());
		}
		return resultList;
	}

	/**
	 * Method to Identify the Search Query Language
	 * 
	 * @param arLiTerms
	 */
	public String getQueryLanguage(ArrayList<String> arLiTerms) {

		// Language String Constants
		String LANG_DE = "DE";
		String LANG_EN = "EN";
		String QueryLanguageResult = null;
		// Variables to hold frequency for each term specific to a Language
		double termFrequencyDE = 0;
		double termFrequencyEN = 0;
		// Variables to hold Total Term Count for All Documents specific to a
		// Language
		double totalTermCountAllDocDE = 0;
		double totalTermCountAllDocEN = 0;
		// Term Occurrence Probability
		double probabilityTermOccurrenceDE = 0;
		double probabilityTermOccurrenceEN = 0;
		// Log Term Occurrence Probability
		double logProbabilityTermOccurrenceDE = 0;
		double logProbabilityTermOccurrenceEN = 0;
		// Final Language Specific Probability Bags
		double deTermProbabilityBag = 0;
		double enTermProbabilityBag = 0;
		// DEFAULT TermCount if Term Count is 0
		int defaultCount = 1;

		try {
			dbConnectionObj.setAutoCommit(true);
			String termCountQuery = "select sum(f.term_frequency) from features f, documents d where f.term = ? and f.docid = d.doc_id  and d.lang = ? group by f.term";
			String totalTermsInAllDocForLanguage = "select sum(f.term_frequency) from features f, documents d where f.docid = d.doc_id and d.lang = ?";
			PreparedStatement psmtTermFreq = dbConnectionObj.prepareStatement(termCountQuery);
			PreparedStatement psmtTotalTermCount = dbConnectionObj.prepareStatement(totalTermsInAllDocForLanguage);
			// Result Sets
			ResultSet rsTotalTermCountAllDocs = null;
			ResultSet rsTotalTermFreqPerDocLanguage = null;

			// Fetch the language specific total term count
			// ENGLISH
			psmtTotalTermCount.setString(1, "EN");
			rsTotalTermCountAllDocs = psmtTotalTermCount.executeQuery();
			if (rsTotalTermCountAllDocs.next()) {
				totalTermCountAllDocEN = rsTotalTermCountAllDocs.getDouble(1);
			}
			GlobalUtilities.PrintLog("TOTAL TERM COUNT IN ALL ENG DOCUMENTS : " + totalTermCountAllDocEN);
			// DEUTSCH
			psmtTotalTermCount.setString(1, "DE");
			rsTotalTermCountAllDocs = psmtTotalTermCount.executeQuery();
			if (rsTotalTermCountAllDocs.next()) {
				totalTermCountAllDocDE = rsTotalTermCountAllDocs.getDouble(1);
			}
			GlobalUtilities.PrintLog("TOTAL TERM COUNT IN ALL DEUTSCH DOCUMENTS : " + totalTermCountAllDocDE);

			// Fetch language specific term frequency values for each term

			for (String sQueryTerm : arLiTerms) {
				// EN
				psmtTermFreq.setString(1, sQueryTerm);
				psmtTermFreq.setString(2, "EN");
				rsTotalTermFreqPerDocLanguage = psmtTermFreq.executeQuery();
				if (rsTotalTermFreqPerDocLanguage.next()) {
					termFrequencyEN = rsTotalTermFreqPerDocLanguage.getDouble(1);
				}
				GlobalUtilities.PrintLog(sQueryTerm + " (EN) : Count = " + termFrequencyEN);

				// DE
				psmtTermFreq.setString(1, sQueryTerm);
				psmtTermFreq.setString(2, "DE");
				rsTotalTermFreqPerDocLanguage = psmtTermFreq.executeQuery();
				if (rsTotalTermFreqPerDocLanguage.next()) {
					termFrequencyDE = rsTotalTermFreqPerDocLanguage.getDouble(1);
				}
				GlobalUtilities.PrintLog(sQueryTerm + " (DE) : Count = " + termFrequencyDE);
				// INCASE NO COUNT RETURNED PUT DEFAULT VALUE AS 1 FOR TERMS
				// THAT DO NOT OCCUR IN DB
				if (termFrequencyEN <= 0) {
					termFrequencyEN = 1;
				}
				if (termFrequencyDE <= 0) {
					termFrequencyDE = 1;
				}

				// Computing Term Occurrence Probabilities
				probabilityTermOccurrenceEN = (termFrequencyEN / totalTermCountAllDocEN);
				probabilityTermOccurrenceDE = (termFrequencyDE / totalTermCountAllDocDE);
				GlobalUtilities.PrintLog(
						"PROBABILITY TERM OCCURRENCE EN : " + sQueryTerm + " : " + probabilityTermOccurrenceEN);
				GlobalUtilities.PrintLog(
						"PROBABILITY TERM OCCURRENCE DE : " + sQueryTerm + " : " + probabilityTermOccurrenceDE);

				// Computing Term Occurrence Logarithmic Probabilities
				logProbabilityTermOccurrenceEN = Math.log(probabilityTermOccurrenceEN);
				logProbabilityTermOccurrenceDE = Math.log(probabilityTermOccurrenceDE);
				GlobalUtilities.PrintLog(
						"LOG PROBABILITY TERM OCCURRENCE EN : " + sQueryTerm + " : " + logProbabilityTermOccurrenceEN);
				GlobalUtilities.PrintLog(
						"LOG PROBABILITY TERM OCCURRENCE DE : " + sQueryTerm + " : " + logProbabilityTermOccurrenceDE);

				// Add Log Probability values to language specific bags
				enTermProbabilityBag = enTermProbabilityBag + logProbabilityTermOccurrenceEN;
				deTermProbabilityBag = deTermProbabilityBag + logProbabilityTermOccurrenceDE;

			}

			GlobalUtilities.PrintLog("FINAL BAG EN : " + enTermProbabilityBag);
			GlobalUtilities.PrintLog("FINAL BAG DE : " + deTermProbabilityBag);

			if (enTermProbabilityBag > deTermProbabilityBag) {
				GlobalUtilities.PrintLog("Query is English");
				QueryLanguageResult = LANG_EN;
			} else {
				GlobalUtilities.PrintLog("Query is Deutsch");
				QueryLanguageResult = LANG_DE;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return QueryLanguageResult;

	}

	public boolean checkIfTermExistsInDB(String sTerm) {
		try {
			String sQuery = "SELECT * from FEATURES where term = ?";
			PreparedStatement psmtTermExists = dbConnectionObj.prepareStatement(sQuery);
			ResultSet rsTermExists = null;
			psmtTermExists.setString(1, sTerm);
			rsTermExists = psmtTermExists.executeQuery();
			if (rsTermExists.next()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			GlobalUtilities.PrintLog("Exception in checkTermMatches" + e.getMessage());
			e.printStackTrace();
		}
		return false;
	}

	public String checkTermMatches(ArrayList<String> arLiSearchTerms, String sLang) {
		GlobalUtilities.PrintLog("checkTermMatches **********************" + arLiSearchTerms.size());
		HashMap<String, ArrayList<String>> mapTermMatches = new HashMap<>();
		String sSuggestedQuery = "";
		try {
			for (String sterm : arLiSearchTerms) {
				if (!checkIfTermExistsInDB(sterm)) {
					ArrayList<String> arLiTermMatch = getTermMatchesForSingleTerm(sterm, sLang);
					if (arLiSearchTerms != null) {
						mapTermMatches.put(sterm, arLiTermMatch);
					} else {
						// In case no term matches exists in DB for All edit
						// distances
						// Possible if DB is empty
						mapTermMatches.put(sterm, null);
					}
				} else {
					// So if Term Exists no need for term matches list
					// So add null for the corresponding term
					mapTermMatches.put(sterm, null);
				}
			}
			// For each term in the map if the list is not null
			// Select the best matches and create a new suggested search query
			ArrayList<String> liSuggestedQueryList = null;

			for (String sTerm : mapTermMatches.keySet()) {
				ArrayList<String> liCandidateTerms = mapTermMatches.get(sTerm);
				if (liCandidateTerms != null) {
					// In case in the list only one or more matching terms
					// exists(same ed same df) is
					// only one
					if (liCandidateTerms.size() == 1) {
						sSuggestedQuery = sSuggestedQuery + liCandidateTerms.get(0) + " ";
					} else {
						// if multiple terms exists
						boolean isFoundClosestMatch = false;
						for (String stringSuggestion : liCandidateTerms) {
							if (stringSuggestion.contains(sTerm)) {
								sSuggestedQuery = sSuggestedQuery + stringSuggestion + " ";
								isFoundClosestMatch = true;
								break;// Just need one term
							} else if (sTerm.contains(stringSuggestion)) {
								sSuggestedQuery = sSuggestedQuery + stringSuggestion + " ";
								isFoundClosestMatch = true;
								break;// Just need one term
							}
						}
						if (!isFoundClosestMatch) {
							sSuggestedQuery = sSuggestedQuery + liCandidateTerms.get(0) + " ";
						}
					}
				} else {// If it already exists in DB add the original term back
						// to the suggested query
					sSuggestedQuery = sSuggestedQuery + sTerm + " ";
				}
			}
			// Clean Up : Trim Leading and Trailing extra spaces
			sSuggestedQuery = sSuggestedQuery.trim();
			GlobalUtilities.PrintLog("checkTermMatches() Suggested Query : " + sSuggestedQuery);

			// checkMatchBetweenOriginalAndSuggestedQuery
			// THIS IS DONE TO AVOID MISMATCH DUE TO RE ORDERING OF THE SAME
			// TERMS
			int checkCounter = 0;
			String[] suggesterTermsArray = sSuggestedQuery.split(" ");
			for (String sOriginalSearchTerm : arLiSearchTerms) {
				for (String sNewSuggestionTerm : suggesterTermsArray) {
					if (sOriginalSearchTerm.equalsIgnoreCase(sNewSuggestionTerm)) {
						checkCounter++;
					}
				}
			}
			// If all the terms present in th original list are present in
			// suggested query then return empty
			if (checkCounter == arLiSearchTerms.size()) {
				sSuggestedQuery = "";
			}

			return sSuggestedQuery;

		} catch (Exception e) {
			GlobalUtilities.PrintLog("Exception in checkTermMatches" + e.getMessage());
			e.printStackTrace();
		}
		return sSuggestedQuery;
	}

	/**
	 * Method to fetch list of term matches for the searched term
	 * 
	 * @param sTerm
	 */
	public ArrayList<String> getTermMatchesForSingleTerm(String sTerm, String sLang) {
		int minEditDistanceThreshold = 1;
		ArrayList<String> arLiTermMatches = null;
		String sLang_1 = null;
		String sLang_2 = null;
		if (sLang.contains(":")) {
			String[] sLangArray = sLang.split(":");
			sLang_1 = sLangArray[0];
			sLang_2 = sLangArray[1];
		} else {
			sLang_1 = sLang;
			sLang_2 = sLang;
		}
		GlobalUtilities.PrintLog("Search Suggested Query Term for Language : " + sLang_1 + ":" + sLang_2);
		// Query to fetch all the terms which have the least edit distance
		// from the search term
		// and which has the highest document frequency since we want to
		// suggest terms to the user
		// which have highest occurrence among all documents
		// String sStringMatchQuery = "select distinct term from features where
		// df=" + "(SELECT max(df) as df"
		// + " FROM features " + "WHERE levenshtein(term, ?) <=? )and
		// levenshtein(term, ?) <= ?";

		// Query to fetch the term/terms which having the maximum df(the term
		// which occurs in most documents)
		// and having the edit distance starting from 1 using fuzzystrmatch
		String sStringMatchQueryWithLang = "select distinct term from features where df = (SELECT max(f.df) as df FROM features f, documents d "
				+ " WHERE levenshtein(f.term, ?) <=? and f.docid = d.doc_id and d.lang in(?,?) )and levenshtein(term, ?) <=?";

		PreparedStatement psmtTermMatch = null;
		ResultSet rsTermMatches = null;
		try {
			psmtTermMatch = dbConnectionObj.prepareStatement(sStringMatchQueryWithLang);
			// While will keep executing till we get results
			// If not it increments editDistance and loops till we get a result
			while (arLiTermMatches == null || arLiTermMatches.size() <= 0) {
				psmtTermMatch.setString(1, sTerm);
				psmtTermMatch.setInt(2, minEditDistanceThreshold);
				psmtTermMatch.setString(3, sLang);
				psmtTermMatch.setString(4, sLang);
				psmtTermMatch.setString(5, sTerm);
				psmtTermMatch.setInt(6, minEditDistanceThreshold);
				// Result Set
				rsTermMatches = psmtTermMatch.executeQuery();
				// In no term exists with the minEditDistance Threshold
				// Increment the threshold and repeat search match function
				if (rsTermMatches.next()) {
					arLiTermMatches = new ArrayList<>();
					do {// Keep adding all the terms(having same df) and with
						// same edit distance as the original term
						arLiTermMatches.add(rsTermMatches.getString(1));
					} while (rsTermMatches.next());
				} else {
					// Result Set is Empty so Increase the Edit Distance
					minEditDistanceThreshold++;
				}
			}

			if (arLiTermMatches != null) {
				GlobalUtilities.PrintLog("TERM SUGGESTIONS for " + sTerm);
				for (String sSuggestTermMatches : arLiTermMatches) {
					GlobalUtilities.PrintLog(sSuggestTermMatches);
				}
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			GlobalUtilities.PrintLog("Exception in getTermMatchesForSingleTerm" + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			GlobalUtilities.PrintLog("Exception in getTermMatchesForSingleTerm" + e.getMessage());
			e.printStackTrace();
		}
		return arLiTermMatches;
	}

	/******************** ADVERTIZERS *********************/
	/**
	 * FETCH TOP K ADVERTISEMENTS
	 * @param searchQuery
	 * @param noOfAds
	 */
	public ArrayList<Advertisement> fetchTopKAdvertisements(String searchQuery, int noOfAds) {
		ArrayList<Advertisement> arAds = new ArrayList<>();
		Advertisement objAd = null;
		try {
			if(searchQuery==null || noOfAds ==0){
				return null;
			}
			
			String[] arSearchTerms = searchQuery.split(" ");
			StringBuilder sbNgramQuery = new StringBuilder();
			ArrayList<String> nGramSearchQuery = new ArrayList<>();
			
			// Create n-grams and add to list
			for (int i = 0; i < arSearchTerms.length; i++) {
				// Initialize nGram Counter to i+0 (to consider terms from
				// current term onwards)
				int iNgramLimitCounter = i + 0;
				// Execute till all indexes in array are consumed
				while (iNgramLimitCounter != arSearchTerms.length) {
					// Add first term
					String nGram = arSearchTerms[i];
					for (int j = i + 1; j <= iNgramLimitCounter; j++) {
						nGram = nGram + "_" + arSearchTerms[j];
					}
					// Add nGram after exiting
					nGramSearchQuery.add(nGram);
					// Increment nGram Counter
					iNgramLimitCounter++;
				}
			}
			//NGRAMS Generated from search query
			for (String nGramTerm : nGramSearchQuery) {
				GlobalUtilities.PrintLog(""+nGramTerm);
				sbNgramQuery.append(nGramTerm+" ");
			}
			String sNgramCombinedQUery = sbNgramQuery.toString();
			GlobalUtilities.PrintLog("NGRAM Combined QUERY:"+sNgramCombinedQUery);
			String sFetchTopKAdsQuery = "SELECT * FROM gettopkAds(string_to_array(?,' '),?)";
			PreparedStatement psmtTopKAds = dbConnectionObj.prepareStatement(sFetchTopKAdsQuery);
			psmtTopKAds.setString(1, sNgramCombinedQUery);
			psmtTopKAds.setInt(2, noOfAds);
			ResultSet rsTopKAdData = psmtTopKAds.executeQuery();
			while(rsTopKAdData.next()){
				objAd = new Advertisement();
				objAd.setAd_id(rsTopKAdData.getInt(1));
				objAd.setAd_score(rsTopKAdData.getInt(2));
				objAd.setAd_rank(rsTopKAdData.getInt(3));
				objAd.setOrganization(rsTopKAdData.getString(4));
				objAd.setAdv_page_url(rsTopKAdData.getString(5));
				objAd.setAdv_img_url(rsTopKAdData.getString(6));
				objAd.setDisplay_text(rsTopKAdData.getString(7));
				objAd.setBudget(rsTopKAdData.getDouble(8));
				objAd.setRate(rsTopKAdData.getDouble(9));
				objAd.setCurrent_budget(rsTopKAdData.getDouble(10));
				//Add to Advertisement List
				arAds.add(objAd);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		GlobalUtilities.PrintLog("No Of Ads Returned:"+arAds.size());
		return arAds;
	}

	/**
	 * INSERT ADVERTISEMENTS
	 * 
	 * @param mapAdData
	 */
	public void insertAdvertisementDataToDB(HashMap<String, String> mapAdData) {

		String strOrganizationName = "";
		String strPageToNavigate = "";
		String strDisplayText = "";
		String strImageURL = "";
		String strKeywords = "";
		ArrayList<String> arLiAdWords = new ArrayList<>();
		Double dBudget = null;
		Double dClickRate = null;
		try {
			if (mapAdData != null) {
				strOrganizationName = mapAdData.get("ORG");
				strPageToNavigate = mapAdData.get("PAGE");
				strDisplayText = mapAdData.get("TEXT");
				strImageURL = mapAdData.get("IMAGE");
				strKeywords = mapAdData.get("KEYWORDS");
				String strBudget = mapAdData.get("BUDGET");
				String strClickRate = mapAdData.get("RATE");
				dBudget = Double.valueOf(strBudget);
				dClickRate = Double.valueOf(strClickRate);
				// Split Ad-words and add to list
				String[] arADwords = strKeywords.split(";");
				// GlobalUtilities.PrintLog("AD WORDS Array
				// length:"+arADwords.length);
				for (String adWord : arADwords) {
					adWord = adWord.trim();
					if (adWord.contains(" ")) {
						adWord = adWord.replace(" ", "_");
					}
					GlobalUtilities.PrintLog("ADWORD:" + adWord);
					// Add to ad word list
					arLiAdWords.add(adWord);
				}
				// INSERT VALUES INTO ADV_DATA
				String queryADV_DATA = "insert into ADV_DATA (ORGANIZATION, AD_PAGE_URL, AD_IMG_URL, AD_DISPLAY_TEXT, AD_BUDGET, AD_CLICK_RATE, AD_CURRENT_BUDGET) "
						+ "values (?, ?, ? ,? ,? ,?, 0)";
				PreparedStatement psmtADV_DATA = dbConnectionObj.prepareStatement(queryADV_DATA);
				psmtADV_DATA.setString(1, strOrganizationName);
				psmtADV_DATA.setString(2, strPageToNavigate);
				psmtADV_DATA.setString(3, strImageURL);
				psmtADV_DATA.setString(4, strDisplayText);
				psmtADV_DATA.setDouble(5, dBudget);
				psmtADV_DATA.setDouble(6, dClickRate);

				psmtADV_DATA.executeUpdate();

				// FETCH AD ID
				String queryFetchADV_ID = "select AD_ID from ADV_DATA where ORGANIZATION = ? AND AD_PAGE_URL = ? AND AD_IMG_URL = ?";
				PreparedStatement psmtAD_ID = dbConnectionObj.prepareStatement(queryFetchADV_ID);
				psmtAD_ID.setString(1, strOrganizationName);
				psmtAD_ID.setString(2, strPageToNavigate);
				psmtAD_ID.setString(3, strImageURL);
				ResultSet rsFetchAD_ID = psmtAD_ID.executeQuery();
				int ADV_ID = 0;
				while (rsFetchAD_ID.next()) {
					ADV_ID = rsFetchAD_ID.getInt(1);
				}
				rsFetchAD_ID.close();
				GlobalUtilities.PrintLog("ADV ID : " + ADV_ID);

				// INSERT VALUE INTO ADV_WORDS
				String queryADV_WORDS = "insert into ADV_WORDS (AD_ID, AD_WORDS) values(?, ?)";
				PreparedStatement psmtADV_WORDS = dbConnectionObj.prepareStatement(queryADV_WORDS);
				for (String adWord : arLiAdWords) {
					psmtADV_WORDS.setInt(1, ADV_ID);
					psmtADV_WORDS.setString(2, adWord);
					psmtADV_WORDS.executeUpdate();
				}

			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/**
	 * UPDATE CLICK AND CURRENT BUDGET FOR ADVERTISEMENTS 
	 * @param AD_ID
	 */
	public String updateAdvertisementBudgetData(String AD_ID){
		String querySelectAdvData = "SELECT ad_click_rate, ad_current_budget, ad_page_url from ADV_DATA where ad_id = ?";
		String queryUpdateCurrentBudget = "UPDATE ADV_DATA SET ad_current_budget = ? where ad_id = ?";
		String adURL = "";
		Double dRate = 0.0;
		Double currentBudget = 0.0;
		Double noOfClicks = 0.0;
		Integer iAdID = Integer.valueOf(AD_ID);
		try {
			PreparedStatement psmtAdv = dbConnectionObj.prepareStatement(querySelectAdvData);
			psmtAdv.setInt(1, iAdID);
			ResultSet rsAdvData = psmtAdv.executeQuery();
			while(rsAdvData.next()){
				dRate = rsAdvData.getDouble(1);
				currentBudget = rsAdvData.getDouble(2);
				adURL = rsAdvData.getString(3);
			}
			
			//Compute current budget
			currentBudget = currentBudget + dRate;
			psmtAdv = dbConnectionObj.prepareStatement(queryUpdateCurrentBudget);
			psmtAdv.setDouble(1, currentBudget);
			psmtAdv.setInt(2, iAdID);
			psmtAdv.executeUpdate();
			
			GlobalUtilities.PrintLog("Updated Current Budget.");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return adURL;
	}

	/**
	 * END PROCESS QUERY FILE
	 */
}
