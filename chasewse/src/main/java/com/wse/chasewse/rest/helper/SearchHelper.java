package com.wse.chasewse.rest.helper;


import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.wse.chasewse.util.*;

import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.SynsetType;
import edu.smu.tspell.wordnet.WordNetDatabase;

import com.wse.chasewse.model.*;


public class SearchHelper {
	//DbHandler
	public ProcessQuery objProcessQuery = new ProcessQuery();
	//WordNetDB
	WordNetDatabase wrdNetdatabase;
	// MODE : CONJUNCTION/DISJUNCTION
	int MODE = 1;
	// Variables
	Stemmer objStemmerFX = null;
	// Set to store English stop word list
	HashSet<String> stStopWordList = new HashSet<>(GlobalUtilities.setEngStopWords);
	//GERMANY Language Synonyms list
	HashMap<String, String> mapGermanSynList = new HashMap<>();
	String LANG_EN = "EN";
	String LANG_DE = "DE";
	String LANG_ALL = "EN DE";
	String QUERY_LANG = LANG_EN;// Defaults to EN
	boolean isLangPreSelected = false;
	String newSearchQuery = null;
	String sOriginalQuery = null;	
	// Collection to hold ResultData Objects
	ArrayList<ResultData> arLiResult = null;
	ArrayList<ImageResultData> arLiImgResultList = null;
	ArrayList<Advertisement> arLiAdvertisementResult = null;
	ArrayList<Stat> statList = null;
	
	// Constructor
	public SearchHelper() {
		// Establish DataBase Connection
		objProcessQuery.createDataBaseConnection();	
		//Instantiate Stemmer
		objStemmerFX = new Stemmer();
		// Location of the English Synonym Dictionary Files
		File wnDictFiles = new File("//Users//***********//WordNet-3.0//dict");
		System.setProperty("wordnet.database.dir", wnDictFiles.toString());
		wrdNetdatabase = WordNetDatabase.getFileInstance();	
		//Location of German Synonym File
		File deSynData = new File("//Users//****************//openthesaurus.txt");
		// Generate the German Synonym List
		mapGermanSynList = GlobalUtilities.generateGermanSynonymList(deSynData);
		//English StopWord List
		File enStopWrdFile = new File("//Users//*******************//stop.txt");
		GlobalUtilities.generateStopwordListFromFile(enStopWrdFile);
	}
	
	/**
	 * Method to process text search request
	 * @param searchRequestObject
	 * @return
	 */
	public ResponseResultData processSearchQuery(QueryData searchRequestObject){

		String searchQuery = searchRequestObject.getSearchQuery();
		int k = searchRequestObject.getTopKDocuments();
		String selectedQueryLang = searchRequestObject.getQueryLang();
		String sQueryLanguage = "";
		//Final search response
		String jsonResponseData = null;
		ResponseResultData finalResult = null;

		ArrayList<String> searchTermsList = null;
		if(searchQuery!=null && searchQuery.length()>0){
			if(k>0){
				//Get cleaned up search query
				String searchQueryClean  = queryCleanUp(searchQuery);
				//Fetch relevant ads
				arLiAdvertisementResult = fetchTopKRelevantAdvertisements(searchQueryClean, 4);
				if(arLiAdvertisementResult!=null && arLiAdvertisementResult.size()>0){
					GlobalUtilities.PrintLog("Number of relevant ads:"+arLiAdvertisementResult.size());
				}else{
					GlobalUtilities.PrintLog("No relevant ads found!");
				}

				//Get the searchTermList
				searchTermsList = new ArrayList<>(Arrays.asList(searchQueryClean.split(" ")));
				GlobalUtilities.PrintLog("The no. of search terms is ....:" + searchTermsList.size());
				/******************* { STEP #1 }LANGUAGE AUTO DETECTION IF LANG NOT PRE-SELECTED ************************/
				GlobalUtilities.PrintLog("userLanguageSelection :" + selectedQueryLang);
				//GET The query language, if not specified auto detect language
				sQueryLanguage = findQueryLangFromRequest(selectedQueryLang, searchTermsList);
				GlobalUtilities.PrintLog("Final Query lang: "+sQueryLanguage);
				/******************* { STEP #2 }FETCH ALL SYNONYMS FOR SEARCH TERMS AND APPEND TO QUERY ************************/
				//Send the ORIGINAL search query along with cleaned up query
				searchQueryClean = fetchAllSynonyms(searchQuery, searchQueryClean);
				GlobalUtilities.PrintLog("Syn-Updated search query clean: " + searchQueryClean);
				/******************* { STEP #3 }STOPWORD AND STEMMING IF LANG IS ENG ************************/
				if(QUERY_LANG.equalsIgnoreCase(LANG_EN)){
					searchQueryClean = stopWordStemmingProcess(searchQueryClean);
				}
				GlobalUtilities.PrintLog("[PostStopwordStemmer]Updated search query clean: " + searchQueryClean);
				/******************* { STEP #4 }FETCH QUERY SUGGESTIONS FROM DB ************************/
				searchTermsList = new ArrayList<>(Arrays.asList(searchQueryClean.split(" ")));
				searchQueryClean = getAutoSuggestionsFromDb(searchQueryClean, searchTermsList);
				GlobalUtilities.PrintLog("[Auto Suggestion]Updated search query clean: " + searchQueryClean);
				/******************* { STEP #5 }Check if required keywords and domain present in query ************************/
				//Send the original search query
				HashMap<String, String> mapKeyDomain = getRequiredKeywordsAndDomain(searchQuery);
				String keywords = mapKeyDomain.get("KEYWORDS");
				String domain = mapKeyDomain.get("DOMAIN");
				GlobalUtilities.PrintLog("Keywords: " + keywords);
				GlobalUtilities.PrintLog("Domain: " + domain);
				/******************* { STEP #6 }Fetch Search Results ************************/
				int model = searchRequestObject.getScoringModel();
				HashMap<String, Object> mapQueryResultData = fetchSearchResults(searchQueryClean, k, model, keywords, domain, searchTermsList);
				/******************* { STEP #7 }Build and Fetch JSON Response ************************/
				//jsonResponseData = getFinalJsonResponsedata(mapQueryResultData, searchQueryClean, k);
				finalResult = buildResponseResultModel(mapQueryResultData, searchQueryClean, k, arLiAdvertisementResult);
			}else{
				GlobalUtilities.PrintLog("Number of documents k is less than 1!");
			}
		}else{
			GlobalUtilities.PrintLog("Search Query is null or empty!");
		}

		return finalResult;
	}

	/**
	 * Method to process image queries
	 * @param searchQuery
	 * @param k
	 * @return
	 */
	public ArrayList<ImageResultData> processImageSearch(String searchQuery, int k){
		//cleanup
		String searchQueryClean = searchQuery.replaceAll("\\s+", " ").toLowerCase();
		// Execute DB Queries
		if (searchQueryClean != null && (!searchQueryClean.isEmpty())) {
			arLiImgResultList = objProcessQuery.imageSearchDB(searchQueryClean, k);
			if (arLiImgResultList != null) {
				GlobalUtilities.PrintLog("SIZE OF IMAGE RESULT:"+arLiImgResultList.size());
			}
		}
		return arLiImgResultList;
	}

	// Helper Methods

	public String queryCleanUp(String searchQuery){
		// SearchQuery for Cleanup and Processing
		// replace all white spaces with single space and
		// convert to
		// lower case
		searchQuery = searchQuery.replaceAll("\\s+", " ").toLowerCase();

		// Get a CleanedUp Version of the searchQuery
		// CleanUp Search Query
		// Remove quotation marks if present
		String searchQueryClean = searchQuery;
		if (searchQueryClean.contains("~")) {
			GlobalUtilities.PrintLog(" Query contains TILDE operator");
			searchQueryClean = searchQueryClean.replace("~", "");
		}
		if (searchQueryClean.contains("\"")) {
			searchQueryClean = searchQueryClean.replace("\"", "");
		}
		// Remove Site Operator
		if (searchQueryClean.contains("site:")) {
			searchQueryClean = searchQueryClean
					.replace(searchQuery.substring(searchQuery.indexOf("site:"),
							searchQuery.indexOf(" ", searchQuery.indexOf("site:"))), "");
		}
		searchQueryClean = searchQueryClean.replaceAll("\\s+", " ").toLowerCase();
		GlobalUtilities.PrintLog("The cleanedup search query is ....:" + searchQueryClean);
		return searchQueryClean;
	}

	public ArrayList<Advertisement> fetchTopKRelevantAdvertisements(String searchQueryClean, int numberOfAds){
		/******************* {FETCH APPROPRIATE ADVERTISEMENTS} ************************/
		int maxNoOfAdsToDisplay = numberOfAds;
		ArrayList<Advertisement> arLiAdvertisementResult = objProcessQuery.fetchTopKAdvertisements(searchQueryClean, maxNoOfAdsToDisplay);

		return arLiAdvertisementResult;
	}

	public String findQueryLangFromRequest(String selectedQueryLang, ArrayList<String> searchTermsList){
		if(selectedQueryLang!=null && selectedQueryLang.length()>0){
			switch (selectedQueryLang.toUpperCase()) {
			case "EN":isLangPreSelected=true;QUERY_LANG=LANG_EN;break;
			case "DE":isLangPreSelected=true;QUERY_LANG=LANG_DE;break;
			case "ANY":isLangPreSelected=false;QUERY_LANG=LANG_ALL;break;
			default:isLangPreSelected=false;QUERY_LANG=LANG_ALL;break;
			}
		}

		//Auto-detect query language if not preselected
		if(!isLangPreSelected){
			QUERY_LANG = langugaeAutoDetect(searchTermsList);
		}
		return QUERY_LANG;
	}

	public String langugaeAutoDetect(ArrayList<String> searchTermsList){
		String autoDetectedLang = "";
		/************ LANG AUTO DETECT *******/ 

		// Query Language Detection
		String sQueryLanguage = objProcessQuery.getQueryLanguage(searchTermsList);
		if (sQueryLanguage != null) {
			if(sQueryLanguage.equalsIgnoreCase("EN")){
				autoDetectedLang = LANG_EN;
			}else if(sQueryLanguage.equalsIgnoreCase("DE")){
				autoDetectedLang = LANG_DE;
			}else{
				autoDetectedLang = LANG_ALL;
			}
		}
		GlobalUtilities.PrintLog("QUERY LANGUAGE IS AUTO DETECTED AS:"+autoDetectedLang);
		return autoDetectedLang;

	}

	public String fetchAllSynonyms(String searchQuery, String searchQueryClean){
		/******************* { STEP #2 }FETCH ALL SYNONYMS FOR SEARCH TERMS AND APPEND TO QUERY ************************/

		// SYNONYMS FOR TERMS APPENDED WITH TILDE~ OPERATOR
		//Using the original search query which contains special characters
		//Add the synonyms to the searchQueryClean which will be used for further processing
		ArrayList<String> additionalSynTermsToBeAdded = new ArrayList<>();
		if (searchQuery.contains("~")) {

			ArrayList<String> arLiCheckForSyn = new ArrayList<>();
			GlobalUtilities.PrintLog("Query contains TILDE, find synonyms");

			String[] sQueryTerms = searchQuery.split(" ");
			for (int i = 0; i < sQueryTerms.length; i++) {
				String sTerm = sQueryTerms[i];
				if (sTerm.contains("~")) {
					String sCleanTerm = sTerm.substring(sTerm.indexOf("~") + 1);
					arLiCheckForSyn.add(sCleanTerm);
				}
			}
			for (String stringToCheck : arLiCheckForSyn) {
				GlobalUtilities.PrintLog("Term To Check for Syn: " + stringToCheck + " Length : "
						+ stringToCheck.length());
				//FETCH THE SYNONYM LIST BASED ON LANGUAGE FOR EACH TERM
				ArrayList<String> synTermsList = getWordNetSynonyms(stringToCheck);
				if (synTermsList != null && synTermsList.size() > 0) {
					additionalSynTermsToBeAdded.addAll(synTermsList);
				} else {
					GlobalUtilities.PrintLog("No Syn Found for the Term:" + stringToCheck);
				}
			}

			// Add all synonyms to SearchQueryClean
			for (String synTerm : additionalSynTermsToBeAdded) {
				searchQueryClean = searchQueryClean + " " + synTerm;
			}
		}

		return searchQueryClean;
	}


	public String stopWordStemmingProcess(String searchQueryClean){
		/******************* { STEP #3 }STOPWORD AND STEMMING IF LANG IS ENG ************************/
		ArrayList<String> searchTermsList = new ArrayList<>(Arrays.asList(searchQueryClean.split(" ")));
		GlobalUtilities.PrintLog("[updated]The no. of search terms is ....:" + searchTermsList.size());

		// LANGUAGE BASED CLEAN UP
		// *********************************
		// Application of Stop word elimination and stemming in
		// case language is ENG
		if (QUERY_LANG.equalsIgnoreCase(LANG_EN)) {
			// Apply STOPWORD Elimination
			for (int ix = 0; ix<searchTermsList.size();ix++) {
				if(GlobalUtilities.setEngStopWords.contains(searchTermsList.get(ix))){
					String sTerm = searchTermsList.get(ix);
					GlobalUtilities.PrintLog("STOPWORD Detected:"+sTerm);
					searchTermsList.remove(ix);
				}
			}
			// Apply STEMMER
			ArrayList<String> arLiSearchTermsUpdated = stemmingTermList(searchTermsList);
			searchTermsList = arLiSearchTermsUpdated;
			// Build the searchQueryClean String from the
			// stemmedSearchTermsList
			searchQueryClean = "";
			for (String sTerm : searchTermsList) {
				GlobalUtilities.PrintLog("Terms after stemming :" + sTerm);
				searchQueryClean = searchQueryClean + " " + sTerm;
			}
			// CleanUp
			searchQueryClean = searchQueryClean.trim();
			GlobalUtilities.PrintLog(
					"Search Query After Stopword and Stemmer Application : " + searchQueryClean);

		} else if (QUERY_LANG.equalsIgnoreCase(LANG_DE)) {
			// NO NEED FOR STOP WORD OR STEMMER
		} else {
			GlobalUtilities.PrintLog("[Stopword Stemmer Process]QUERY LANGUAGE NOT IDENTIFIED");
			// ALL LANG
			QUERY_LANG = LANG_ALL;
		}
		return searchQueryClean;
	}

	public String getAutoSuggestionsFromDb(String searchQueryClean, ArrayList<String> searchTermsList){
		/***
		 * Checking Query Suggestions
		 */
		// Query Terms Suggestion
		// Send the Query LANG as a parameter to get term
		// suggestions in the selected language
		String sSuggestedSearchQuery = objProcessQuery.checkTermMatches(searchTermsList, QUERY_LANG);
		// Will return the same string if no other suggestion
		if (sSuggestedSearchQuery != null && sSuggestedSearchQuery.length() > 0) {
			// keeping a reference for the original query
			sOriginalQuery = searchQueryClean;
			newSearchQuery = sSuggestedSearchQuery;
			if (searchQueryClean.equalsIgnoreCase(sSuggestedSearchQuery)) {
				GlobalUtilities.PrintLog("SAME SUGGESTION AS THE ENTERED QUERY");
				// newSearchQuery = sSuggestedSearchQuery;//
			} else {
				// newSearchQuery = sSuggestedSearchQuery;
			}
			GlobalUtilities.PrintLog("Original Search Query : " + sOriginalQuery + " Length :"
					+ sOriginalQuery.length());
			GlobalUtilities.PrintLog("Suggested Search Query : " + sSuggestedSearchQuery + " Length :"
					+ sSuggestedSearchQuery.length());

			// If there exists a better search query -- TO DO
			// for eg. incase the req no of documents k = 20
			// and there are similar terms with edDist =1 having
			// higher df then
			// suggest to user : did u mean : .....

			// In case if misspelling replace it entirely
			// Display the original and suggested new query
			// Showing results for -----
			searchQueryClean = sSuggestedSearchQuery;

			/**
			 * After new query suggestions again language check
			 * needs to be done for the new query
			 */
			// Only if Query Language is not specified by the
			// user
			if (!isLangPreSelected) {
				String[] newSearchterms = searchQueryClean.split(" ");
				ArrayList<String> newSearchTermsList = new ArrayList<String>(
						Arrays.asList(newSearchterms));
				String sQueryLanguage = objProcessQuery.getQueryLanguage(newSearchTermsList);
				if (sQueryLanguage != null) {
					QUERY_LANG = sQueryLanguage;
				}
				GlobalUtilities.PrintLog("SUGGESTED QUERY LANGUAGE IS : " + QUERY_LANG);
			}
		} else {
			//IF NO SUGGESTIONS THEN THE ORIGINAL SEARCH QUERY IS USED
			sOriginalQuery = searchQueryClean;
			newSearchQuery = searchQueryClean;
		}

		return searchQueryClean;
	}

	public HashMap<String, String> getRequiredKeywordsAndDomain(String searchQuery){
		String keywords = null;
		String domain = null;
		HashMap<String, String> mapKeyDomain = new HashMap<>();
		// Fetch the Keywords if exists(Term required in all
		// documents)
		if (searchQuery.contains("\"")) {
			GlobalUtilities.PrintLog("Query contains quotation");
			keywords = searchQuery.substring(searchQuery.indexOf("\"") + 1,
					searchQuery.lastIndexOf("\""));
			GlobalUtilities.PrintLog("Keywords are....:" + keywords);
		}
		// fetch domain if site operator exists
		if (searchQuery.contains("site:")) {
			int start = searchQuery.indexOf("site:");
			domain = searchQuery.substring(searchQuery.indexOf("site:") + 5,
					searchQuery.indexOf(" ", start));
			GlobalUtilities.PrintLog("The domain is ....:" + domain);
		} else {
			domain = "%";
			GlobalUtilities.PrintLog("The domain is empty , hence ....:" + domain);
		}
		mapKeyDomain.put("KEYWORDS", keywords);
		mapKeyDomain.put("DOMAIN", domain);
		return mapKeyDomain;
	}

	public HashMap<String, Object> fetchSearchResults(String searchQueryClean, int k, int model, String keywords, String domain, ArrayList<String> searchTermsList){
		ArrayList<ResultData> arLiResult = null;
		HashMap<String, Object> mapQueryResultData = new HashMap<>();
		// Execute DB Queries
		//MODEL 4 is for SNIPPET SCORE + TFIDF
		if(!(model == 4)){
			if (keywords != null && (!keywords.isEmpty())) {
				arLiResult = objProcessQuery.keywordSearch(searchQueryClean, keywords, domain, k,
						QUERY_LANG, model);
			} else {
				arLiResult = objProcessQuery.normalSearch(searchQueryClean, domain, k, QUERY_LANG, model);
			}
		}else{//MODEL 4 is for SNIPPET SCORE + TFIDF
			if (keywords != null && (!keywords.isEmpty())) {
				arLiResult = objProcessQuery.snippetKeywordSearch(searchQueryClean,keywords, domain, k, QUERY_LANG, 1);
			} else {
				arLiResult = objProcessQuery.snippetNormalSearch(searchQueryClean, domain, k, QUERY_LANG, 1);
			}
		}

		GlobalUtilities.PrintLog("RESULT SIZE:" + arLiResult.size());
		// Use the Search Term List to fetch Query Statistics
		// from
		// DataBase
		statList = objProcessQuery.getStat(searchTermsList);
		// GET CW
		String sCW = String.valueOf(objProcessQuery.getCW());
		mapQueryResultData.put("RESULT_DATA_LIST", arLiResult);
		mapQueryResultData.put("STAT_DATA", statList);
		mapQueryResultData.put("CW", sCW);

		return mapQueryResultData;
	}
	public ResponseResultData buildResponseResultModel(HashMap<String, Object> mapFinalResultData, String searchQueryClean, int k, ArrayList<Advertisement> liAds){
		ResponseQueryData objQueryData = new ResponseQueryData();
		objQueryData.setSearchQuery(searchQueryClean);
		objQueryData.setTopKDocuments(k);
		mapFinalResultData.put("QUERY", objQueryData);
		
		ResponseResultData objResponseData = new ResponseResultData();
		objResponseData.setQuery(objQueryData);
		objResponseData.setResultList((ArrayList<ResultData>) mapFinalResultData.get("RESULT_DATA_LIST"));
		objResponseData.setCw(Integer.valueOf((String) mapFinalResultData.get("CW")));
		objResponseData.setStat((ArrayList<Stat>)mapFinalResultData.get("STAT_DATA"));
		objResponseData.setAdvList(liAds);
		
		return objResponseData;
	}
	public String getFinalJsonResponsedata(HashMap<String, Object> mapFinalResultData, String searchQueryClean, int k){

		String jsonStringResponse = null;
		//ResponseQueryObject
		ResponseQueryData objQueryData = new ResponseQueryData();
		objQueryData.setSearchQuery(searchQueryClean);
		objQueryData.setTopKDocuments(k);
		mapFinalResultData.put("QUERY", objQueryData);

		// Build JSON Result Data using MapResultData
		JSONObject objFinalResultJSONData = buildResultJSONData(mapFinalResultData);
		if (objFinalResultJSONData != null) {
			jsonStringResponse = (objFinalResultJSONData.toJSONString());
		}
		return jsonStringResponse;
	}

	/**
	 * Method to fetch all synonyms for terms with tilde operator
	 * 
	 * @param sTerm
	 * @return
	 */
	public ArrayList<String> getWordNetSynonyms(String sTerm) {

		GlobalUtilities.PrintLog("Method to check for Synonyms Start, for Lang:"+QUERY_LANG);
		ArrayList<String> synTermList = new ArrayList<>();
		ArrayList<String> alOriginalList = new ArrayList<String>();
		String mainQueryTerm = sTerm;

		if (QUERY_LANG.equalsIgnoreCase(LANG_EN) || QUERY_LANG.equalsIgnoreCase(LANG_ALL)) {
			// Fetch all the positions for the search term
			Synset[] objSynsetsNOUN = wrdNetdatabase.getSynsets(mainQueryTerm, SynsetType.NOUN);
			Synset[] objSynsetsVERB = wrdNetdatabase.getSynsets(mainQueryTerm, SynsetType.VERB);
			Synset[] objSynsetsADJECTIVE = wrdNetdatabase.getSynsets(mainQueryTerm, SynsetType.ADJECTIVE);
			Synset[] objSynsetsADVERB = wrdNetdatabase.getSynsets(mainQueryTerm, SynsetType.ADVERB);

			// Add all syn sets to list
			ArrayList<Synset[]> arLiSynSetCollection = new ArrayList<>();
			arLiSynSetCollection.add(objSynsetsNOUN);
			arLiSynSetCollection.add(objSynsetsVERB);
			arLiSynSetCollection.add(objSynsetsADJECTIVE);
			arLiSynSetCollection.add(objSynsetsADVERB);

			// HashSet to Remove duplicate synonym elements
			HashSet setSynTerms = new HashSet();

			// Iterate over each Synset and add to array list
			for (Synset[] objSynsets : arLiSynSetCollection) {
				if (objSynsets != null && objSynsets.length > 0) {
					for (int i = 0; i < objSynsets.length; i++) {
						// Fetch all the word forms from the set
						String[] arWordSyn = objSynsets[i].getWordForms();
						// Add to main arraylist
						for (int j = 0; j < arWordSyn.length; j++) {
							alOriginalList.add(arWordSyn[j]);
						}
					}
				}
			}
			// Add the array list to hash set to remove duplicate synonyms
			// and add the cleaned up list back to array list
			if (alOriginalList.size() > 0) {
				// removing duplicates
				setSynTerms.addAll(alOriginalList);
				alOriginalList.clear();
				alOriginalList.addAll(setSynTerms);
			} else {
				GlobalUtilities.PrintLog("No Syn exists for the term : " + mainQueryTerm);
			}

		} else if (QUERY_LANG.equalsIgnoreCase(LANG_DE)) {

			if (mapGermanSynList != null && mapGermanSynList.size() > 0) {
				if (mapGermanSynList.containsKey(mainQueryTerm)) {
					String sSynTermString = mapGermanSynList.get(mainQueryTerm);
					if (sSynTermString != null && sSynTermString.length() > 0) {
						if (sSynTermString.contains(";")) {
							String[] strSynArray = sSynTermString.split(";");
							for (int i = 0; i < strSynArray.length; i++) {
								String sSynTerm = strSynArray[i];
								sSynTerm = sSynTerm.trim();
								// add to the syn Arraylist
								alOriginalList.add(sSynTerm);
							}
						} else {
							sSynTermString = sSynTermString.trim();
							alOriginalList.add(sSynTermString);
						}
					}
				} else {
					GlobalUtilities.PrintLog("No Syn for Deutsch Term Exists : " + mainQueryTerm);
				}
			}
		}
		// Remove the search term from the list
		for (int j = 0; j < alOriginalList.size(); j++) {
			GlobalUtilities.PrintLog("List of Synonyms for : " + mainQueryTerm);
			GlobalUtilities.PrintLog(alOriginalList.get(j));
			String synToBeAddedToList = alOriginalList.get(j);
			// Do not add term same as search term
			if (!synToBeAddedToList.equalsIgnoreCase(mainQueryTerm)) {
				synToBeAddedToList = synToBeAddedToList.toLowerCase();
				synTermList.add(synToBeAddedToList);
			}
		}
		return synTermList;
	}

	
	
	
	/**
	 * Build JSON RESULT Object
	 * 
	 * @param mapData
	 * @return
	 */
	public JSONObject buildResultJSONData(HashMap<String, Object> mapData) {
		ArrayList<ResultData> arLiResultDataList = null;
		ArrayList<Stat> arLiStatList = null;
		String CW = null;
		ResponseQueryData objQueryData = null;
		JSONObject rootLevelJSON = null;
		try {
			// Populate data variables from map
			if (mapData.containsKey("RESULT_DATA_LIST")) {
				arLiResultDataList = (ArrayList<ResultData>) mapData.get("RESULT_DATA_LIST");
			}
			if (mapData.containsKey("STAT_DATA")) {
				arLiStatList = (ArrayList<Stat>) mapData.get("STAT_DATA");
			}
			if (mapData.containsKey("CW")) {
				CW = (String) mapData.get("CW");
			}
			if (mapData.containsKey("QUERY")) {
				objQueryData = (ResponseQueryData) mapData.get("QUERY");
			}

			// Create JSON Object
			if (arLiResultDataList != null && arLiStatList != null) {
				// Create Root Level JSON Object
				rootLevelJSON = new JSONObject();

				// Result List
				// Create JSON Array to hold Result JSON Objects
				JSONArray objResultListJsonArray = new JSONArray();
				HashMap<String, String> mapRDData = null;
				for (ResultData objRD : arLiResultDataList) {
					mapRDData = new HashMap<>();
					mapRDData.put("rank", String.valueOf(objRD.getRank()));
					mapRDData.put("url", String.valueOf(objRD.getUrl()));
					mapRDData.put("score", String.valueOf(objRD.getScore()));
					// Invoke buildJSONObj to build JSON Object for each result
					// entry
					objResultListJsonArray.add(buildJSONObj(mapRDData));
				}
				// Add the Result JSON Array to Root JSON Object
				rootLevelJSON.put("resultList", objResultListJsonArray);

				// Statistics List
				JSONArray objStatListJsonArray = new JSONArray();
				HashMap<String, String> mapStatData = null;
				for (Stat objStat : arLiStatList) {
					mapStatData = new HashMap<>();
					mapStatData.put("term", String.valueOf(objStat.getTerm()));
					mapStatData.put("df", String.valueOf(objStat.getDf()));
					// Invoke buildJSONObj to build JSON Object for each
					// Statistic Object entry
					objStatListJsonArray.add(buildJSONObj(mapStatData));
				}
				// Add the Statistics JSON Array to Root JSON Object
				rootLevelJSON.put("stat", objStatListJsonArray);

				// Query JSON Object
				HashMap<String, String> mapQueryData = new HashMap<>();
				mapQueryData.put("k", String.valueOf(objQueryData.getTopKDocuments()));
				mapQueryData.put("query", objQueryData.getSearchQuery());
				rootLevelJSON.put("query", buildJSONObj(mapQueryData));

				// CW JSON Object
				rootLevelJSON.put("cw", Integer.valueOf(CW));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return rootLevelJSON;

	}

	/**
	 * Build Child JSON Objects From Map
	 * 
	 * @param mapJSONObjDataKeyValues
	 */
	public JSONObject buildJSONObj(HashMap<String, String> mapJSONObjDataKeyValues) {
		JSONObject objJSON = null;
		if (mapJSONObjDataKeyValues != null) {
			objJSON = new JSONObject();
			for (String sKey : mapJSONObjDataKeyValues.keySet()) {
				if(sKey.equalsIgnoreCase("DF")||sKey.equalsIgnoreCase("CW")||sKey.equalsIgnoreCase("K")||sKey.equalsIgnoreCase("RANK")){
					objJSON.put(sKey,Integer.valueOf(mapJSONObjDataKeyValues.get(sKey)));
				}else if(sKey.equalsIgnoreCase("SCORE")){
					objJSON.put(sKey,Double.valueOf(mapJSONObjDataKeyValues.get(sKey)));
				}else{
					objJSON.put(sKey, mapJSONObjDataKeyValues.get(sKey));
				}
			}
		}
		return objJSON;
	}

	/**
	 * Method for Stemming Terms
	 * 
	 * @param arLiTermListToBeStemmed
	 */
	public ArrayList<String> stemmingTermList(ArrayList<String> arLiTermListToBeStemmed) {
		ArrayList<String> stemmedDataList = null;
		if (arLiTermListToBeStemmed != null) {
			stemmedDataList = new ArrayList<>();
			for (String sTerm : arLiTermListToBeStemmed) {
				if (sTerm != null && sTerm.length() != 0) {
					char[] chData = sTerm.toCharArray();
					objStemmerFX.add(chData, chData.length);
					objStemmerFX.stem();
					String updatedData = objStemmerFX.toString();

					// Add to new List
					stemmedDataList.add(updatedData);
					// System.out.println(i+" : "+updatedData+" :
					// "+updatedData.length());
				} else {
					// System.out.println("ALERT EMPTY STRING");
				}
			}
		}
		return stemmedDataList;
	}

	/**
	 * Method to eliminate stop words from Eng Term List
	 * 
	 * @param arTermList
	 * @return
	 */
	public ArrayList<String> eliminateStopWordsFromTermDataList(String[] arTermList) {
		ArrayList<String> arLiUpdatedTermDataList = new ArrayList<>();
		if (arTermList != null && arTermList.length > 0) {
			for (int iCount = 0; iCount < arTermList.length; iCount++) {
				String sTerm = arTermList[iCount];
				if (!(stStopWordList.contains(sTerm))) {
					arLiUpdatedTermDataList.add(sTerm);
				} else {
					System.out.println("REMOVED STOPWORD : " + sTerm);
				}
			}
		}
		return arLiUpdatedTermDataList;
	}
}
