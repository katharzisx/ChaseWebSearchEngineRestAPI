package com.wse.chasewse.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import com.wse.chasewse.util.GlobalUtilities;




public class ResultData {
	private int rank;
	private String url;
	private double score;
	private String description;
	private String MissingTerms;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMissingTerms() {
		return MissingTerms;
	}

	public void setMissingTerms(String missingTerms) {
		MissingTerms = missingTerms;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public void getMissingTermsFromResult(String searchString, String occured_terms) {
		String missingTerms = "";
		HashSet<String> setMissingTerms = new HashSet<>();
		String occured_terms_clean = occured_terms.replaceAll("[{}\"]", "");
		GlobalUtilities.PrintLog("SEARCH TERM " + searchString);
		GlobalUtilities.PrintLog("OCCUR TERM " + occured_terms_clean);

		ArrayList<String> occured_term_list = new ArrayList<>();
		ArrayList<String> searchString_list = new ArrayList<>();

		String[] arTermsOccurred = occured_terms_clean.split(",");
		String[] arTermsSearch = searchString.split(" ");
		for (String sTerm : arTermsOccurred) {
			occured_term_list.add(sTerm);
		}
		for (String sTerm : arTermsSearch) {
			searchString_list.add(sTerm);
		}
		
		
		
//		GlobalUtilities.PrintLog("ORIGINAL TERM LIST");
//		for (int i = 0; i < searchString_list.size(); i++) {
//			GlobalUtilities.PrintLog("" + searchString_list.get(i));
//		}
//		GlobalUtilities.PrintLog("OCCURRED TERM LIST");
//		for (int i = 0; i < occured_term_list.size(); i++) {
//			GlobalUtilities.PrintLog("" + occured_term_list.get(i));
//		}

		for (String string : occured_term_list) {
			setMissingTerms.add(string);
		}
		
		searchString_list.removeAll(setMissingTerms);
		for (int i = 0; i < searchString_list.size(); i++) {
			missingTerms = missingTerms + searchString_list.get(i) + " ";
		}
		if (missingTerms != null && missingTerms.length() > 0) {
			missingTerms = missingTerms.trim();
		} else {
			missingTerms = "";
		}
		GlobalUtilities.PrintLog("MISSING TERMS :"+missingTerms);
		//Set to string
		setMissingTerms(missingTerms);

	}
}
