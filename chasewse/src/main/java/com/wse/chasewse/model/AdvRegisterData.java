package com.wse.chasewse.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="adv_data")
public class AdvRegisterData implements Serializable{
	private static final long serialVersionUID = -3009157732242241606L;
	
	@Column(name = "ad_id")
	@Id
    private long ad_id;
	@Column(name = "organization")
	String organization;
	@Column(name = "ad_page_url")
	String ad_page_url;
	@Column(name = "ad_img_url")
	String ad_img_url;
	@Column(name = "ad_display_text")
	String ad_display_text;
	@Column(name = "ad_budget")
	Long ad_budget;
	@Column(name = "ad_click_rate")
	Double ad_click_rate;
	@Column(name = "ad_current_budget")
	Double ad_current_budget;
	
	String keywords;
	
	
	public AdvRegisterData(Long ad_id, String organization, String ad_page_url, String ad_display_text, 
			String ad_img_url, String keywords, Long ad_budget, Double ad_current_budget, Double ad_click_rate){
		this.ad_id = ad_id;
		this.organization = organization;
		this.ad_page_url = ad_page_url;
		this.ad_img_url = ad_img_url;
		this.ad_display_text = ad_display_text;
		this.keywords = keywords;
		this.ad_budget = ad_budget;
		this.ad_current_budget = ad_current_budget;
		this.ad_click_rate = ad_click_rate;
	}
	
	AdvRegisterData() {}//JPA

	public Long getAd_id() {
		return ad_id;
	}

	public void setAd_id(Long ad_id) {
		this.ad_id = ad_id;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getAd_page_url() {
		return ad_page_url;
	}

	public void setAd_page_url(String ad_page_url) {
		this.ad_page_url = ad_page_url;
	}

	public String getAd_img_url() {
		return ad_img_url;
	}

	public void setAd_img_url(String ad_img_url) {
		this.ad_img_url = ad_img_url;
	}

	public String getAd_display_text() {
		return ad_display_text;
	}

	public void setAd_display_text(String ad_display_text) {
		this.ad_display_text = ad_display_text;
	}

	public Long getAd_budget() {
		return ad_budget;
	}

	public void setAd_budget(Long ad_budget) {
		this.ad_budget = ad_budget;
	}

	public Double getAd_click_rate() {
		return ad_click_rate;
	}

	public void setAd_click_rate(Double ad_click_rate) {
		this.ad_click_rate = ad_click_rate;
	}

	public Double getAd_current_budget() {
		return ad_current_budget;
	}

	public void setAd_current_budget(Double ad_current_budget) {
		this.ad_current_budget = ad_current_budget;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
}
