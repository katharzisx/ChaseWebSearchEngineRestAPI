package com.wse.chasewse.model;

import java.util.ArrayList;

import io.swagger.annotations.ApiModelProperty;

public class ResponseResultData {
	ArrayList<ResultData> resultList;
	ArrayList<Stat> stat;
	Integer cw;
	ResponseQueryData query;
	ArrayList<Advertisement> advList;
	
	public ArrayList<Advertisement> getAdvList() {
		return advList;
	}
	public void setAdvList(ArrayList<Advertisement> advList) {
		this.advList = advList;
	}
	public ArrayList<Stat> getStat() {
		return stat;
	}
	public void setStat(ArrayList<Stat> stat) {
		this.stat = stat;
	}
	
    @ApiModelProperty(notes = "The query fired.", required = true)
	public ResponseQueryData getQuery() {
		return query;
	}
	public void setQuery(ResponseQueryData query) {
		this.query = query;
	}
	public ArrayList<ResultData> getResultList() {
		return resultList;
	}
	public void setResultList(ArrayList<ResultData> resultList) {
		this.resultList = resultList;
	}

	public Integer getCw() {
		return cw;
	}
	public void setCw(Integer cw) {
		this.cw = cw;
	}	
}
