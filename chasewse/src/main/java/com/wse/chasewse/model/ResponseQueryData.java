package com.wse.chasewse.model;

public class ResponseQueryData {
	String searchQuery;
	int topKDocuments;
	public String getSearchQuery() {
		return searchQuery;
	}
	public void setSearchQuery(String searchQuery) {
		this.searchQuery = searchQuery;
	}
	public int getTopKDocuments() {
		return topKDocuments;
	}
	public void setTopKDocuments(int topKDocuments) {
		this.topKDocuments = topKDocuments;
	}
	
}
