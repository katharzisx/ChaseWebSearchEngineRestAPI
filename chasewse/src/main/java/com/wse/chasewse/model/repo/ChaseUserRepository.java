package com.wse.chasewse.model.repo;

import org.springframework.data.repository.CrudRepository;

import com.wse.chasewse.model.ChaseUser;

public interface ChaseUserRepository extends CrudRepository<ChaseUser, Long>{

	public ChaseUser findByEmail(String email);
}
