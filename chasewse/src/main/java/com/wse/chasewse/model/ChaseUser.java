package com.wse.chasewse.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="user_data")
public class ChaseUser implements Serializable{	
	private static final long serialVersionUID = 1L;
	
	@Column(name = "user_id",nullable = false)
	@Id
    private long user_id;
	@Column(name = "username",nullable = false)
	String username;
	@Column(name = "password",nullable = false)
	String password;
	@Column(name = "email",nullable = false)
	String email;
	
	
	ChaseUser() {
		// JPA
	}
	
	public ChaseUser(Long user_id, String username, String password, String email){
		this.user_id = user_id;
		this.username = username;
		this.password = password;
		this.email = email;
	}
	
	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
