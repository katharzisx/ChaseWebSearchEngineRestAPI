package com.wse.chasewse.model.repo;
import java.util.List;

import org.springframework.data.repository.*;

import com.wse.chasewse.model.AdvRegisterData;
public interface AdvertisementRepository extends CrudRepository<AdvRegisterData, Long>{
	List<AdvRegisterData> findByOrganization(String organization);
}
