package com.wse.chasewse.model;

public class Advertisement {
	int ad_id;
	int ad_score;
	int ad_rank;
	String organization;
	String adv_page_url;
	String adv_img_url;
	String display_text;
	double budget;
	double rate;
	double current_budget;
	
	public int getAd_id() {
		return ad_id;
	}
	public void setAd_id(int ad_id) {
		this.ad_id = ad_id;
	}
	public int getAd_score() {
		return ad_score;
	}
	public void setAd_score(int ad_score) {
		this.ad_score = ad_score;
	}
	public int getAd_rank() {
		return ad_rank;
	}
	public void setAd_rank(int ad_rank) {
		this.ad_rank = ad_rank;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public String getAdv_page_url() {
		return adv_page_url;
	}
	public void setAdv_page_url(String adv_page_url) {
		this.adv_page_url = adv_page_url;
	}
	public String getAdv_img_url() {
		return adv_img_url;
	}
	public void setAdv_img_url(String adv_img_url) {
		this.adv_img_url = adv_img_url;
	}
	public String getDisplay_text() {
		return display_text;
	}
	public void setDisplay_text(String display_text) {
		this.display_text = display_text;
	}
	public double getBudget() {
		return budget;
	}
	public void setBudget(double budget) {
		this.budget = budget;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public double getCurrent_budget() {
		return current_budget;
	}
	public void setCurrent_budget(double current_budget) {
		this.current_budget = current_budget;
	}
}
