package com.wse.chasewse.model;

public class ImageResultData {
	//CREATE type img_holder as (doc_url text,doc_id text, img_id text, img_url text, score real);
	private String doc_url;
	private String doc_id;
	private String img_id;
	private String img_url;
	private double score;
	private int rank;
	
	public String getDoc_url() {
		return doc_url;
	}
	public void setDoc_url(String doc_url) {
		this.doc_url = doc_url;
	}
	public String getDoc_id() {
		return doc_id;
	}
	public void setDoc_id(String doc_id) {
		this.doc_id = doc_id;
	}
	public String getImg_id() {
		return img_id;
	}
	public void setImg_id(String img_id) {
		this.img_id = img_id;
	}
	public String getImg_url() {
		return img_url;
	}
	public void setImg_url(String img_url) {
		this.img_url = img_url;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	
}
