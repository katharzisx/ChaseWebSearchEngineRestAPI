package com.wse.chasewse.model;

public class QueryData {
	String searchQuery;
	int topKDocuments;
	int scoringModel;
	String queryLang;
	String resultType;
	
	public QueryData(String searchQuery,int topKDocuments, int scoringModel, String queryLang, String resultType){
		this.searchQuery = searchQuery;
		this.topKDocuments = topKDocuments;
		this.scoringModel = scoringModel;
		this.queryLang = queryLang;
		this.resultType = resultType;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("\nQuery: "+getSearchQuery());
		sb.append("\nK: "+getTopKDocuments());
		sb.append("\nScoring model: "+getScoringModel());
		sb.append("\nLang: "+getQueryLang());
		sb.append("\nResult type: "+getResultType());
		return sb.toString();
	}
	
	public String getSearchQuery() {
		return searchQuery;
	}
	public void setSearchQuery(String searchQuery) {
		this.searchQuery = searchQuery;
	}
	public int getTopKDocuments() {
		return topKDocuments;
	}
	public void setTopKDocuments(int topKDocuments) {
		this.topKDocuments = topKDocuments;
	}
	public int getScoringModel() {
		return scoringModel;
	}
	public void setScoringModel(int scoringModel) {
		this.scoringModel = scoringModel;
	}
	public String getQueryLang() {
		return queryLang;
	}
	public void setQueryLang(String queryLang) {
		this.queryLang = queryLang;
	}
	public String getResultType() {
		return resultType;
	}
	public void setResultType(String resultType) {
		this.resultType = resultType;
	}
}
