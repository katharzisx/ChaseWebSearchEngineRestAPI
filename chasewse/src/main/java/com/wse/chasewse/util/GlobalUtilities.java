package com.wse.chasewse.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;


public class GlobalUtilities {
	public static Set<String> setEngStopWords = Collections.synchronizedSet(new HashSet<>());
	final static Logger logger = Logger.getLogger(GlobalUtilities.class);
	/**
	 * Method to generate StopWord Set
	 */
	public static void generateStopwordListFromFile(File stopTextData) {
		try {
			// Read stop words from text file
			FileReader fReaderStopWord = new FileReader(stopTextData);
			BufferedReader bReaderStopWord = new BufferedReader(fReaderStopWord);
			String line;
			String stStopWord;
			while ((line = bReaderStopWord.readLine()) != null) {
				if (line.contains("|")) {
					line = line.substring(0, line.indexOf("|"));
					stStopWord = line.trim();
					if (stStopWord != null && stStopWord.length() > 0) {
						setEngStopWords.add(stStopWord);
					}
				} else {
					stStopWord = line.trim();
					if (stStopWord != null && stStopWord.length() > 0) {
						setEngStopWords.add(stStopWord);
					}
				}
			}
			bReaderStopWord.close();
			PrintLog("SIZE OF STOPWORD SET : " + setEngStopWords.size());
			// for (String string : stStopWordList) {
			// System.out.println(string);
			// }

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Method to Generate German Synonym List
	 */
	public static HashMap<String, String> generateGermanSynonymList(File deSynFile) {
		HashMap<String, String> mapGermanSynList = new HashMap<>();
		try {
			int counter = 0;
			// Fetch the text file into a stream
//			BufferedReader bReaderGermanSyn = new BufferedReader(
//					new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("/openthesaurus.txt")));
			FileInputStream deFileIS = 	new FileInputStream(deSynFile);
			InputStreamReader isr = new InputStreamReader(deFileIS);
			BufferedReader bReaderGermanSyn = new BufferedReader(isr);
			//BufferedReader bReaderGermanSyn = new BufferedReader(new InputStreamReader(deSynFile.));
			String sline;
			// String stWord;
			while ((sline = bReaderGermanSyn.readLine()) != null) {
				// Do not consider lines starting with #
				if (!sline.contains("#")) {
					// Remove ... occurrences
					if (sline.contains(".")) {
						sline.replace(".", "");
					}

					// To Lower Case
					sline = sline.toLowerCase();

					// Remove () bracket and its contents from line if exists
					String regEx = "\\([^)]*\\)";
					sline = sline.replaceAll(regEx, "");
					if (sline != null && sline.length() > 0) {

						String[] sSynTerm = sline.split(";");
						// Add each term into the HashMap as a key
						// and the complete line as value for each Synonym
						for (int i = 0; i < sSynTerm.length; i++) {
							String strSynTerm = sSynTerm[i];
							strSynTerm = strSynTerm.trim();
							if (!mapGermanSynList.containsKey(strSynTerm)) {
								mapGermanSynList.put(strSynTerm, sline);
							}
						}
					}
				}
			}
			GlobalUtilities.PrintLog("German Syn List Size : " + mapGermanSynList.size());

			bReaderGermanSyn.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return mapGermanSynList;
	}
	
	
	
	/**
	 * Print Log Messages with TimeStamp
	 * @param msg
	 */
	public static void PrintLog(String msg) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		// get current date time with Date()
		Date date = new Date();
		String logMsg = "[" + dateFormat.format(date) + "] : " + msg;
		logger.info(logMsg);
		System.out.println(logMsg);
	}
	
}
