package com.wse.chasewse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ChasewseApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChasewseApplication.class, args);
	}
	
}
