package com.wse.chasewse;

import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import static org.junit.Assert.*;

import com.wse.chasewse.model.AdvRegisterData;
import com.wse.chasewse.model.repo.AdvertisementRepository;
import org.springframework.web.context.WebApplicationContext;


import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChasewseApplication.class)
@WebAppConfiguration
public class AdvertisementControllerTest {
	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));
	private MockMvc mockMvc;
	private HttpMessageConverter mappingJackson2HttpMessageConverter;
	AdvRegisterData adRegData;
	@Autowired
    private AdvertisementRepository advertisementRepository;
	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
            .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
            .findAny()
            .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }
	
	@Before
	public void initSetup() throws Exception{
		this.mockMvc = webAppContextSetup(webApplicationContext).build();
		this.adRegData = advertisementRepository.save(new AdvRegisterData(111L, "testorg","pagurl;","displytxt","imgurl", "testkeywords", 9L, 19.0, 2.0));
	}
	
	@Test
	public void readOrganisationAds() throws Exception{
		//http://localhost:8080/chase/chasewse/ads/testorg/getAds
		this.mockMvc.perform(get("/chase/chasewse/ads/"+adRegData.getOrganization()+"/getAds"));
		//.andExpect(status().isOk());
	}
	
	@Test
	public void createNewAdv() throws Exception{
		AdvRegisterData newAdvData = new AdvRegisterData(919L,"asdcorp", "asdcorp.org","text","asdcorp.org/logo.png", "asdcorpkeywords", 101L, 21.0, 1.3);
		String adJson = json(newAdvData);
		//http://localhost:8080/chase/chasewse/ads/register
		this.mockMvc.perform(post("/chase/chasewse/ads/register")
				.contentType("application/json")
				.content(adJson));
				//.andExpect(status().isCreated())
				//.andExpect(status().isOk());
	}
	
	@Test
	public void testDeleteAdv() throws Exception{
		//http://localhost:8080/chase/chasewse/ads/delete/testorg
		mockMvc.perform(delete("/chase/chasewse/ads/delete/"+adRegData.getOrganization()));
		//.andExpect(status().isOk());
	}
	
	protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }
	
}
